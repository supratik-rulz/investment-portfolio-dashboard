
(function (factory) {
  if (typeof module === 'object' && typeof module.exports !== "undefined") {
      module.exports = factory;
  } else {
      factory();
  }
}(function () {
    if (arguments.length) {
        setTimeout(function () {
            throw new Error(
                "A deprecated way of resolving FusionCharts as a dependency is detected. Please use FusionCharts.addDep instead. For more information, please visit https://www.fusioncharts.com/dev/getting-started/plain-javascript/install-using-plain-javascript#install-fusioncharts-via-npm-5"
            );
        }, 0);
    }
(window["webpackJsonpFusionCharts"] = window["webpackJsonpFusionCharts"] || []).push([["fusioncharts.msstackedcolumn2dsplinedy"],{

/***/ "./develop/src/_internal/factories/msstackedcolumn-spline-dataset.js":
/*!***************************************************************************!*\
  !*** ./develop/src/_internal/factories/msstackedcolumn-spline-dataset.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nexports.__esModule = true;\n\nexports['default'] = function (chart) {\n  var jsonData = chart.getFromEnv('dataSource'),\n      dataset = jsonData.dataset,\n      splineSets = jsonData.lineset || [],\n      splinesetStartIndex = void 0,\n      indices = void 0,\n      canvas = chart.getChildren('canvas')[0],\n      vCanvas = canvas.getChildren('vCanvas')[1];\n\n  if (!dataset && splineSets.length === 0) {\n    chart.setChartMessage();\n    return;\n  }\n  (0, _msstackedcolumnDataset2['default'])(chart);\n  splinesetStartIndex = chart.config._lastDatasetIndex + 1;\n  if (splineSets && splineSets.length) {\n    indices = Array(splineSets.length).fill(splinesetStartIndex).map(function (n, j) {\n      return n + j;\n    });\n    (0, _lib.datasetFactory)(vCanvas, chart.getDSdef('spline'), 'dataset_spline', splineSets.length, splineSets, indices);\n  } else {\n    removeLineSet(vCanvas);\n  }\n};\n\nvar _msstackedcolumnDataset = __webpack_require__(/*! ./msstackedcolumn-dataset */ \"./develop/src/_internal/factories/msstackedcolumn-dataset.js\");\n\nvar _msstackedcolumnDataset2 = _interopRequireDefault(_msstackedcolumnDataset);\n\nvar _lib = __webpack_require__(/*! ../lib/lib */ \"./develop/src/_internal/lib/lib.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }\n\nvar removeLineSet = function removeLineSet(component) {\n  var lineSet = component.getChildren('dataset_line'),\n      i = void 0;\n  for (i = lineSet && lineSet.length - 1; i > -1; i--) {\n    lineSet[i].remove();\n  }\n};\n/**\n * function to  create dataset, groupmaneger.\n * assign dataset to group manager.\n * @param {Object} chart Chart API\n **/\n\n//# sourceURL=webpack://FusionCharts/./develop/src/_internal/factories/msstackedcolumn-spline-dataset.js?");

/***/ }),

/***/ "./develop/src/msstackedcolumn2dsplinedy/index.js":
/*!********************************************************!*\
  !*** ./develop/src/msstackedcolumn2dsplinedy/index.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nexports.__esModule = true;\nexports.MSStackedColumn2DSplineDY = undefined;\n\nvar _msstackedcolumn2dsplinedy = __webpack_require__(/*! ../viz/msstackedcolumn2dsplinedy */ \"./develop/src/viz/msstackedcolumn2dsplinedy.js\");\n\nvar _msstackedcolumn2dsplinedy2 = _interopRequireDefault(_msstackedcolumn2dsplinedy);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }\n\nexports.MSStackedColumn2DSplineDY = _msstackedcolumn2dsplinedy2['default'];\nexports['default'] = {\n  name: 'msstackedcolumn2dsplinedy',\n  type: 'package',\n  requiresFusionCharts: true,\n  extension: function extension(FusionCharts) {\n    return FusionCharts.addDep(_msstackedcolumn2dsplinedy2['default']);\n  }\n};\n\n//# sourceURL=webpack://FusionCharts/./develop/src/msstackedcolumn2dsplinedy/index.js?");

/***/ }),

/***/ "./develop/src/viz/msstackedcolumn2dsplinedy.js":
/*!******************************************************!*\
  !*** ./develop/src/viz/msstackedcolumn2dsplinedy.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nexports.__esModule = true;\n\nvar _msstackedcolumn2dlinedy = __webpack_require__(/*! ./msstackedcolumn2dlinedy */ \"./develop/src/viz/msstackedcolumn2dlinedy.js\");\n\nvar _msstackedcolumn2dlinedy2 = _interopRequireDefault(_msstackedcolumn2dlinedy);\n\nvar _msspline = __webpack_require__(/*! ../_internal/datasets/msspline */ \"./develop/src/_internal/datasets/msspline.js\");\n\nvar _msspline2 = _interopRequireDefault(_msspline);\n\nvar _column = __webpack_require__(/*! ../_internal/datasets/column */ \"./develop/src/_internal/datasets/column.js\");\n\nvar _column2 = _interopRequireDefault(_column);\n\nvar _cartesian = __webpack_require__(/*! ../_internal/datasets/groups/cartesian.stack */ \"./develop/src/_internal/datasets/groups/cartesian.stack.js\");\n\nvar _cartesian2 = _interopRequireDefault(_cartesian);\n\nvar _msstackedcolumnSplineDataset = __webpack_require__(/*! ../_internal/factories/msstackedcolumn-spline-dataset */ \"./develop/src/_internal/factories/msstackedcolumn-spline-dataset.js\");\n\nvar _msstackedcolumnSplineDataset2 = _interopRequireDefault(_msstackedcolumnSplineDataset);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }\n\nfunction _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); } /* eslint require-jsdoc: 'error', valid-jsdoc: [\"error\", { \"requireReturn\": false }] */\n\n\n/**\n * class definition for MSStackedColumn2DSplineDy chart api\n */\nvar MSStackedColumn2DSplineDy = function (_MSStackedColumn2DLin) {\n  _inherits(MSStackedColumn2DSplineDy, _MSStackedColumn2DLin);\n\n  /**\n   * constructor fn\n   */\n  function MSStackedColumn2DSplineDy() {\n    _classCallCheck(this, MSStackedColumn2DSplineDy);\n\n    var _this = _possibleConstructorReturn(this, _MSStackedColumn2DLin.call(this));\n\n    _this.stack100percent = 0;\n    _this.hasLineSet = true;\n    _this.lineset = true;\n    _this.registerFactory('dataset', _msstackedcolumnSplineDataset2['default'], ['vCanvas']);\n    return _this;\n  }\n  /**\n   * Sets the name of the component\n   * @return {string} name\n   */\n\n\n  MSStackedColumn2DSplineDy.prototype.getName = function getName() {\n    return 'MSStackedColumn2DSplineDy';\n  };\n\n  /**\n   * Provides the name of the chart extension\n   *\n   * @static\n   * @return {string} The name of the chart extension\n   */\n\n\n  MSStackedColumn2DSplineDy.getName = function getName() {\n    return 'MSStackedColumn2DSplineDy';\n  };\n\n  /**\n   * This sets the default configuration\n   * @memberof MSStackedColumn2DSplineDy\n   */\n\n\n  MSStackedColumn2DSplineDy.prototype.__setDefaultConfig = function __setDefaultConfig() {\n    _MSStackedColumn2DLin.prototype.__setDefaultConfig.call(this);\n    var config = this.config;\n    config.sDefaultDatasetType = 'spline';\n    config.friendlyName = 'Multi-series Dual Y-Axis Stacked Column and Line Chart';\n    config.defaultDatasetType = 'column';\n  };\n  /**\n   * This method return the dataset definations for this charts\n   * @param  {string} name type of dataset class\n   * @return {Object}      dataset class\n   */\n\n\n  MSStackedColumn2DSplineDy.prototype.getDSdef = function getDSdef(name) {\n    return name === 'spline' ? _msspline2['default'] : _column2['default'];\n  };\n  /**\n   * This method return the dataset-group definations for this charts\n   * @param  {string} name is type of dataset group\n   * @return {Object} <dataset group class>     dataset group class\n   */\n\n\n  MSStackedColumn2DSplineDy.prototype.getDSGroupdef = function getDSGroupdef() {\n    return _cartesian2['default'];\n  };\n\n  return MSStackedColumn2DSplineDy;\n}(_msstackedcolumn2dlinedy2['default']);\n\nexports['default'] = MSStackedColumn2DSplineDy;\n\n//# sourceURL=webpack://FusionCharts/./develop/src/viz/msstackedcolumn2dsplinedy.js?");

/***/ })

}]);
}));
