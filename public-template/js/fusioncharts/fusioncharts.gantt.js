
(function (factory) {
  if (typeof module === 'object' && typeof module.exports !== "undefined") {
      module.exports = factory;
  } else {
      factory();
  }
}(function () {
    if (arguments.length) {
        setTimeout(function () {
            throw new Error(
                "A deprecated way of resolving FusionCharts as a dependency is detected. Please use FusionCharts.addDep instead. For more information, please visit https://www.fusioncharts.com/dev/getting-started/plain-javascript/install-using-plain-javascript#install-fusioncharts-via-npm-5"
            );
        }, 0);
    }
(window["webpackJsonpFusionCharts"] = window["webpackJsonpFusionCharts"] || []).push([["fusioncharts.gantt"],{

/***/ "./develop/src/gantt/index.js":
/*!************************************!*\
  !*** ./develop/src/gantt/index.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nexports.__esModule = true;\nexports.Gantt = undefined;\n\nvar _gantt = __webpack_require__(/*! ../viz/gantt */ \"./develop/src/viz/gantt.js\");\n\nvar _gantt2 = _interopRequireDefault(_gantt);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }\n\nexports.Gantt = _gantt2['default'];\nexports['default'] = {\n  name: 'gantt',\n  type: 'package',\n  requiresFusionCharts: true,\n  extension: function extension(FusionCharts) {\n    return FusionCharts.addDep(_gantt2['default']);\n  }\n};\n\n//# sourceURL=webpack://FusionCharts/./develop/src/gantt/index.js?");

/***/ })

}]);
}));
