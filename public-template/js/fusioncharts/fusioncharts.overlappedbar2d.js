
(function (factory) {
  if (typeof module === 'object' && typeof module.exports !== "undefined") {
      module.exports = factory;
  } else {
      factory();
  }
}(function () {
    if (arguments.length) {
        setTimeout(function () {
            throw new Error(
                "A deprecated way of resolving FusionCharts as a dependency is detected. Please use FusionCharts.addDep instead. For more information, please visit https://www.fusioncharts.com/dev/getting-started/plain-javascript/install-using-plain-javascript#install-fusioncharts-via-npm-5"
            );
        }, 0);
    }
(window["webpackJsonpFusionCharts"] = window["webpackJsonpFusionCharts"] || []).push([["fusioncharts.overlappedbar2d"],{

/***/ "./develop/src/overlappedbar2d/index.js":
/*!**********************************************!*\
  !*** ./develop/src/overlappedbar2d/index.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nexports.__esModule = true;\nexports.OverlapperBar2D = undefined;\n\nvar _overlappedbar2d = __webpack_require__(/*! ../viz/overlappedbar2d */ \"./develop/src/viz/overlappedbar2d.js\");\n\nvar _overlappedbar2d2 = _interopRequireDefault(_overlappedbar2d);\n\nvar _index = __webpack_require__(/*! ../features/crossline-adapter/index */ \"./develop/src/features/crossline-adapter/index.js\");\n\nvar _index2 = _interopRequireDefault(_index);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }\n\nexports.OverlapperBar2D = _overlappedbar2d2['default'];\nexports['default'] = {\n  name: 'overlappedbar2d',\n  type: 'package',\n  requiresFusionCharts: true,\n  extension: function extension(FusionCharts) {\n    FusionCharts.addDep(_index2['default']);\n    FusionCharts.addDep(_overlappedbar2d2['default']);\n  }\n};\n\n//# sourceURL=webpack://FusionCharts/./develop/src/overlappedbar2d/index.js?");

/***/ }),

/***/ "./develop/src/viz/overlappedbar2d.js":
/*!********************************************!*\
  !*** ./develop/src/viz/overlappedbar2d.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nexports.__esModule = true;\n\nvar _msbarcartesian = __webpack_require__(/*! ./msbarcartesian */ \"./develop/src/viz/msbarcartesian.js\");\n\nvar _msbarcartesian2 = _interopRequireDefault(_msbarcartesian);\n\nvar _bar2d = __webpack_require__(/*! ../_internal/datasets/bar2d */ \"./develop/src/_internal/datasets/bar2d.js\");\n\nvar _bar2d2 = _interopRequireDefault(_bar2d);\n\nvar _multiseriesDataset = __webpack_require__(/*! ../_internal/factories/multiseries-dataset */ \"./develop/src/_internal/factories/multiseries-dataset.js\");\n\nvar _multiseriesDataset2 = _interopRequireDefault(_multiseriesDataset);\n\nvar _column = __webpack_require__(/*! ../_internal/datasets/groups/column.overlapped */ \"./develop/src/_internal/datasets/groups/column.overlapped.js\");\n\nvar _column2 = _interopRequireDefault(_column);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }\n\nfunction _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); } /* eslint require-jsdoc: 'error', valid-jsdoc: [\"error\", { \"requireReturn\": false }] */\n\n\n/**\n * class definition for overlapped bar chart API\n */\nvar OverlappedBar2D = function (_MSBarCartesian) {\n  _inherits(OverlappedBar2D, _MSBarCartesian);\n\n  /**\n   * constructor fn\n   */\n  function OverlappedBar2D() {\n    _classCallCheck(this, OverlappedBar2D);\n\n    var _this = _possibleConstructorReturn(this, _MSBarCartesian.call(this));\n\n    _this.isBar = true;\n    _this.registerFactory('dataset', _multiseriesDataset2['default'], ['vCanvas']);\n    return _this;\n  }\n  /**\n   * parse defualt configuration of the chart\n   * @memberof OverlappedBar2D\n   */\n\n\n  OverlappedBar2D.prototype.__setDefaultConfig = function __setDefaultConfig() {\n    _MSBarCartesian.prototype.__setDefaultConfig.call(this);\n    var config = this.config;\n    config.friendlyName = 'Multi-series Bar Chart';\n    config.hasLegend = true;\n    config.defaultDatasetType = 'bar2d';\n  };\n  /**\n   * Sets the name of the component\n   * @return {string} name\n   */\n\n\n  OverlappedBar2D.prototype.getName = function getName() {\n    return 'OverlappedBar2D';\n  };\n\n  /**\n   * Provides the name of the chart extension\n   *\n   * @static\n   * @return {string} The name of the chart extension\n   */\n\n\n  OverlappedBar2D.getName = function getName() {\n    return 'OverlappedBar2D';\n  };\n\n  /**\n   * This method return the dataset definations for this charts\n   * @return {class} overlappedbar dataset class definition\n   */\n\n\n  OverlappedBar2D.prototype.getDSdef = function getDSdef() {\n    return _bar2d2['default'];\n  };\n  /**\n   * This method return the dataset-group definations for this charts\n   * @return {class} manager API for overlap column\n   */\n\n\n  OverlappedBar2D.prototype.getDSGroupdef = function getDSGroupdef() {\n    return _column2['default'];\n  };\n\n  return OverlappedBar2D;\n}(_msbarcartesian2['default']);\n\nexports['default'] = OverlappedBar2D;\n\n//# sourceURL=webpack://FusionCharts/./develop/src/viz/overlappedbar2d.js?");

/***/ })

}]);
}));
