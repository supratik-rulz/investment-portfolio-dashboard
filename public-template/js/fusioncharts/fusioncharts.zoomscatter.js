
(function (factory) {
  if (typeof module === 'object' && typeof module.exports !== "undefined") {
      module.exports = factory;
  } else {
      factory();
  }
}(function () {
    if (arguments.length) {
        setTimeout(function () {
            throw new Error(
                "A deprecated way of resolving FusionCharts as a dependency is detected. Please use FusionCharts.addDep instead. For more information, please visit https://www.fusioncharts.com/dev/getting-started/plain-javascript/install-using-plain-javascript#install-fusioncharts-via-npm-5"
            );
        }, 0);
    }
(window["webpackJsonpFusionCharts"] = window["webpackJsonpFusionCharts"] || []).push([["fusioncharts.zoomscatter"],{

/***/ "./develop/src/zoomscatter/index.js":
/*!******************************************!*\
  !*** ./develop/src/zoomscatter/index.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nexports.__esModule = true;\nexports.ZoomScatter = undefined;\n\nvar _zoomscatter = __webpack_require__(/*! ../viz/zoomscatter */ \"./develop/src/viz/zoomscatter.js\");\n\nvar _zoomscatter2 = _interopRequireDefault(_zoomscatter);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }\n\nexports.ZoomScatter = _zoomscatter2['default'];\nexports['default'] = {\n  name: 'zoomscatter',\n  type: 'package',\n  requiresFusionCharts: true,\n  extension: function extension(FusionCharts) {\n    FusionCharts.addDep(_zoomscatter2['default']);\n  }\n};\n\n//# sourceURL=webpack://FusionCharts/./develop/src/zoomscatter/index.js?");

/***/ })

}]);
}));
